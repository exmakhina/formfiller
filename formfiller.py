#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-incometaxreturn@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import typing as t
import copy
import collections

from exmakhina.rangespec.rangespec import sequence_from_rangespec


logger = logging.getLogger(__name__)


def collect_data(metadata: dict, data: dict) -> t.Sequence[t.Mapping]:
	"""
	"""
	pages = set()
	for rangespec in metadata.keys():
		for page in sequence_from_rangespec(str(rangespec)):
			pages.add(page-1)

	pages = sorted(tuple(pages))

	logger.info("Pages: %s", pages)

	res = dict()

	for page in pages:
		res[page] = collections.defaultdict(dict)

	for rangespec, rangespec_info in metadata.items():
		for page in sequence_from_rangespec(str(rangespec)):
			for k, v in rangespec_info.items():
				if k == "fields":
					res[page-1][k].update(copy.deepcopy(v))
				else:
					res[page-1][k] = v

	for rangespec, rangespec_info in data.items():
		if rangespec == "-":
			field_pages = (page + 1 for page in pages)
		else:
			field_pages = (page for page in sequence_from_rangespec(str(rangespec)))

		for page in field_pages:
			for name, value in rangespec_info.items():
				field = res[page-1]["fields"].get(name)
				if field is None:
					continue
				field["value"] = value

	return res.values()
