#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-incometaxreturn@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import typing as t
import logging
import re
import numbers


logger = logging.getLogger(__name__)

LengthSpec = str | numbers.Number

def parse_1d(x: LengthSpec):
	if isinstance(x, numbers.Number):
		return float(x)

	if not isinstance(x, str):
		raise TypeError(f"Wanted a string, got {type(x)} for {x}")

	m = re.match("(?P<value>[0-9.]+)(?P<unit>in|pt|mm|cm)?", x)

	if m is None:
		raise ValueError(f"Invalid length: {x}")

	value = float(m.group("value"))

	unit = m.group("unit")

	mul = {
	 "pt": 1,
	 "mm": 72/25.4,
	 "cm": 72/2.54,
	 "in": 72,
	 None: 1,
	}

	return value * mul[unit]

def parse_2d(xy: t.Tuple[LengthSpec,LengthSpec]) -> t.Tuple[float,float]:
	x, y = xy
	return parse_1d(x), parse_1d(y)

