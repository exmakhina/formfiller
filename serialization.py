#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-incometaxreturn@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import io
import decimal

logger = logging.getLogger(__name__)


def construct_decimal(ctor, node):
	return decimal.Decimal(node.value)


def load(path):
	if path.endswith((".yml", ".yaml")):
		from ruamel.yaml import YAML

		class YamlDecimal(YAML):
			def __init__(self):
				super().__init__()
				self.Constructor.add_constructor(
				 'tag:yaml.org,2002:float',
				 construct_decimal,
				)

		yaml = YamlDecimal()
		yaml.allow_duplicate_keys = True
		with io.open(path, "r") as fi:
			return yaml.load(fi)
	elif path.endswith(".toml"):
		import toml
		with io.open(path, "r") as fi:
			return toml.load(fi)["data"]
	elif path.endswith(".json"):
		import json
		with io.open(path, "r") as fi:
			return json.load(fi, parse_float=decimal.Decimal)
	else:
		raise NotImplementedError(path)


def save(obj, path):
	if path.endswith((".yml", ".yaml")):
		from ruamel.yaml import YAML
		yaml = YAML()
		yaml.default_flow_style = None # leaf sequences inline
		with io.open(path, "w") as fo:
			return yaml.dump(obj, fo)
	elif path.endswith(".toml"):
		import json
		import toml
		obj = json.loads(json.dumps(obj))
		obj = dict(data=obj)
		with io.open(path, "w") as fo:
			return toml.dump(obj, fo)
	elif path.endswith(".json"):
		import json
		with io.open(path, "w") as fo:
			return json.dump(obj, fo,
			 indent=" ",
			 ensure_ascii=False,
			)
	else:
		raise NotImplementedError(path)

