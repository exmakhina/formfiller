#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-incometaxreturn@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import typing as t
from collections import defaultdict

from jschon import create_catalog, JSON, JSONSchema

from .serialization import load


logger = logging.getLogger(__name__)


create_catalog("2020-12")


def validate_schema(data, schema):
	schema = JSONSchema(schema)
	result = schema.evaluate(JSON(data))
	if str(result) != "valid":
		raise ValueError(f"Validation failure: {result}")


def load_metadata(path) -> dict:
	schema = load(os.path.join(os.path.dirname(__file__), "schema_metadata.yaml"))
	validate_schema(metadata, schema)
	logger.info("Metadata loaded and validated")
	return metadata

def load_data(path) -> dict:
	schema = load(os.path.join(os.path.dirname(__file__), "schema_data.yaml"))
	data = load(path)
	validate_schema(data, schema)
	logger.info("Metadata loaded and validated")
	return data
