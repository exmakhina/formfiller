.. SPDX-FileCopyrightText: 2024 Jérôme Carretero
   <cJ-formfiller@zougloub.eu>
   & contributors
.. SPDX-License-Identifier: AGPL-3.0-only

##########
FormFiller
##########


Usage
#####

.. code:: sh

   $formfiller output.pdf metadata.yml data.yml


Form Data Specification
#######################


Lengths are expressed using a string containing an unit suffix
(eg. 13pt), or by a plain integer ; in the latter case, the unit
is ``pt`` (PDF pt, being 1/72").


Metadata
********

The schema holds all metadata for fields.
See ``schema_metadata.yaml`` for more details.


For an example, see `<examples/metadata_example_good.yaml>`_.

.. include:: examples/metadata_example_good.yaml
   :code: yaml

Data
****

The schema holds all data for fields.
See ``schema_data.yaml`` for more details.

For an example, see `<examples/data_example_good.yaml>`_.

.. include:: examples/data_example_good.yaml
   :code: yaml


Design
######

While most forms are PDF documents, forms don't necessarily need to be PDFs,
we are just considering a medium with pages.

Filling forms is performed using the cairo library, so there are
many output possibilities (not only PDF, that would be doable say
if PDF was output).

