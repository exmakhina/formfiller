#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-incometaxreturn@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import typing as t

def lorem_ipsum(metadata) -> t.Mapping:
	"""
	Fill a form with some data, in order to show how it can be done.
	"""
	raise NotImplementedError()

