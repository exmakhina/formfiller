#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-incometaxreturn@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import io
import tempfile

from .serialization import *


logger = logging.getLogger(__name__)


def test_load_yaml():
	with tempfile.NamedTemporaryFile(prefix="formfiller", suffix=".yaml") as f:
		f.write(b"a: 261.46\nb: 1\nc: hello\n")
		f.flush()

		data = load(f.name)

		assert data == {
		 "a": decimal.Decimal("261.46"),
		 "b": 1,
		 "c": "hello",
		}


def test_load_json():
	with tempfile.NamedTemporaryFile(prefix="formfiller", suffix=".json") as f:
		f.write(b'{"a": 261.46, "b": 1, "c": "hello" }')
		f.flush()

		data = load(f.name)

		assert data == {
		 "a": decimal.Decimal("261.46"),
		 "b": 1,
		 "c": "hello",
		}
