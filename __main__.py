#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-incometaxreturn@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
import argparse

from .draw import draw
from .serialization import load
from .formfiller import collect_data


logger = logging.getLogger()


def main(argv=None):
	parser = argparse.ArgumentParser(
	 description="Form filling tool",
	)

	parser.add_argument("--log-level",
	 default="WARNING",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)
	parser.add_argument("output",
	 help="output file path/name"
	)
	parser.add_argument("metadata",
	 help="fields metadata file",
	)
	parser.add_argument("data",
	 help="fields data file",
	)
	parser.add_argument("--base",
	 help="Document to be filled (if not specified, things are drawn on blank pages)",
	)

	def do_fill(args):
		logger.info("Loading...")

		metadata = load(args.metadata)
		data = load(args.data)
		logger.debug("Metadata: %s", metadata)
		logger.debug("Data: %s", data)

		data_join = collect_data(metadata, data)
		logger.info("Pages: %d", len(data_join))

		draw(data_join, args.output, args.base)

	parser.set_defaults(func=do_fill)

	args = parser.parse_args(argv)
	logging.basicConfig(
		datefmt="%Y%m%dT%H%M%S",
		level=getattr(logging, args.log_level),
		format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)

	return args.func(args)


if __name__ == "__main__":
	main()
