import logging

from ruamel.yaml import YAML
from jschon import create_catalog, JSON, JSONSchema


logger = logging.getLogger(__name__)


def test_validate_metadata_schema():
	yaml = YAML()
	create_catalog('2020-12')

	with open('schema_metadata.yaml') as schema_yaml:
		data = yaml.load(schema_yaml)
		schema = JSONSchema(data)

	with open("examples/metadata_example_good.yaml") as example_yaml:
		data = yaml.load(example_yaml)
		example_good = JSON(data)

	with open("examples/metadata_example_bad.yaml") as example_yaml:
			data = yaml.load(example_yaml)
			example_bad = JSON(data)

	result = schema.evaluate(example_good)

	# logger.info("Validation: %s", result)

	assert str(result) == "valid"

	result = schema.evaluate(example_bad)

	# logger.info("Validation: %s", result)

	assert str(result) == "invalid"


def test_validate_data_schema():
	yaml = YAML()
	create_catalog('2020-12')

	with open("schema_data.yaml") as schema_yaml:
		data = yaml.load(schema_yaml)
		schema = JSONSchema(data)

	with open("examples/data_example_good.yaml") as example_yaml:
		data = yaml.load(example_yaml)
		example_good = JSON(data)

	with open("examples/data_example_bad.yaml") as example_yaml:
		data = yaml.load(example_yaml)
		example_bad = JSON(data)

	result = schema.evaluate(example_good)

	# logger.info("Validation: %s", result)

	assert str(result) == "valid"

	result = schema.evaluate(example_bad)

	# logger.info("Validation: %s", result)

	assert str(result) == "invalid"



if __name__ == "__main__":
	test_validate_metadata_schema()
	test_validate_data_schema()
