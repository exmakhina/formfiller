#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-incometaxreturn@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import io
import os
import logging
import tempfile
import typing
import urllib.parse

import pypdf
import cairo

from .length import *


logger = logging.getLogger(__name__)


default_paper_size = "8.5in", "11in"
default_font_size = 8
default_font = "Cousine"


def draw_field(name, value, meta, ctx):
	"""
	Draw a single field on a cairo context
	"""

	logger.info("Drawing field: %s=%s (meta %s)",
	 name, value, meta)

	origin = meta.get("origin")
	if origin is not None:
		x, y = origin = parse_2d(origin)

	bbox = meta.get("bounding-box")
	if bbox is not None:
		l, t, w, h = bbox = [parse_1d(_) for _ in bbox]

		show_bbox = meta.get("debug-show-bbox")
		if show_bbox is not None:
			ctx.save()
			try:
				width, (r, g, b, a) = show_bbox
				ctx.set_source_rgba(r, g, b, a)
				ctx.set_line_width(1)
			except:
				pass
			ctx.rectangle(l, t, w, h)
			ctx.stroke()
			ctx.restore()

	align = meta.get("align", -1)
	align_v = meta.get("align-v", -1)

	if origin is None and bbox is not None:
		bl, bt, bw, bh = bbox
		x, y = origin = bl + bw/2, bt + bh/2

	if isinstance(value, typing.Mapping):
		"""
		"""
		ctx.save()

		content_type = value["content-type"]

		def get_data(encoded, cte):
			if cte == "base64":
				import base64
				return base64.b64decode(encoded)
			raise NotImplementedError(cte)

		url = value.get("url")
		if url is None:
			content_transfer_encoding = value.get("content-transfer-encoding", "base64")
			data = get_data(value["data"], content_transfer_encoding)
		else:
			u = urllib.parse.urlparse(url)
			if u.scheme == "file":
				with io.open(u.path, "rb") as fi:
					data = fi.read()
			else:
				raise NotImplementedError(u.scheme)

		if content_type == "image/png":
			with tempfile.TemporaryDirectory() as tmpdir:
				path = os.path.join(tmpdir, "image.png")
				with io.open(path, "wb") as fo:
					fo.write(data)
				surface = cairo.ImageSurface.create_from_png(path)

				if bbox is not None:
					bl, bt, bw, bh = bbox
					iw, ih = surface.get_width(), surface.get_height()
					ar = iw/ih
					rw = bw/iw
					rh = bh/ih
					scale = min(rh, rw)
					ctx.translate(bl, bt)
					ctx.scale(scale, scale)
				else:
					# use origin
					ctx.translate(x, y)

				ctx.set_source_surface(surface, 0, 0)
				ctx.paint()
		else:
			raise NotImplementedError(content_type)

		ctx.restore()
		return

	fmt = meta.get("format")
	if fmt:
		try:
			value = format(value, fmt)
		except ValueError as e:
			raise ValueError(f"When trying to draw field {name}, can't format “{value}” of type {type(value)} using {fmt}") from e
	else:
		value = str(value)

	ctx.save()

	color = meta.get("font-color")
	if color is None:
		ctx.set_source_rgb(0, 0, 0)
	else:
		ctx.set_source_rgba(*color)

	font_f = meta.get("font-family")
	if font_f is None:
		font_f = default_font
	ctx.select_font_face(font_f)

	font_s = meta.get("font-size")
	if font_s is None:
		font_s = default_font_size
	ctx.set_font_size(parse_1d(font_s))

	logger.debug("Font: %s size %s color %s", font_f)


	if origin is None:
		if bbox is None:
			raise ValueError("Unspecified origin")
		l, t, w, h = bbox
		origin = (l + w/2, t + h/2)
		align = "center"

	char_spacing = meta.get("character-spacing")
	if char_spacing is not None:
		char_spacing = parse_1d(char_spacing)
	d = meta.get("decimal-separator", ".")
	if meta.get("decimal-hidden"):
		value = value.replace(d, " ")
		d = " "

	x_bearing, y_bearing, width, height, dx, dy = ctx.text_extents(value)
	if align in ("left", -1):
		pass
	elif align in ("right", +1):
		x -= width
	elif align == "decimal":
		#ctx.move_to(x, y)
		#ctx.show_text(",")

		a, *b = value.split(d)

		# Measure the width of the part before the decimal point
		(x_bearing, y_bearing, width_before, height, dx, dy) = t_ext = ctx.text_extents(a + d)
		(x_bearing, y_bearing, width_before, height, dx, dy) = d_ext = ctx.text_extents(d)

		# Calculate starting position for the text to make decimal point at desired position
		if char_spacing is not None:
			x = x - char_spacing * len(a)
		else:
			x = x - t_ext.width - t_ext.x_bearing + d_ext.width / 2

		if meta.get("decimal-hidden"):
			x -= d_ext.x_advance / 2

	elif align in ("center", 0):
		x -= width/2

	if align_v in ("center", 0):
		y += height/2

	ctx.move_to(x, y)

	line_spacing = meta.get("line-spacing", font_s)

	if char_spacing is not None:
		seen_d = False
		for idx_letter, letter in enumerate(value):
			(x_bearing, y_bearing, width, height, dx, dy) = ctx.text_extents(letter)
			if align == "decimal":
				if letter == d:
					dx += -char_spacing/2
					seen_d = True
				elif not seen_d:
					dx += 0
				else:
					dx += -char_spacing
			else:
				dx = 0
			ctx.move_to(x + dx + idx_letter * char_spacing - x_bearing - width/2, y)
			ctx.show_text(letter)
	else:
		for idx_line, line in enumerate(value.splitlines()):
			ctx.move_to(x, y + line_spacing * idx_line)
			ctx.show_text(line)
	ctx.restore()


def draw_info(info, ctx) -> None:
	"""
	"""
	for field_name, field_meta in info["fields"].items():
		value = field_meta.get("value")
		if value is None:
			logger.info("Skipping field %s", field_name)
			continue
		try:
			draw_field(field_name, value, field_meta, ctx)
		except Exception as e:
			raise Exception(f"When trying to draw field {field_name}, got {e}") from e


def add_page(info, path) -> None:
	"""
	Create PDF page at specified path, with provided form filling info
	"""
	w, h = parse_2d(info.get("size", default_paper_size))
	ps = cairo.PDFSurface(path, w, h)
	ctx = cairo.Context(ps)
	return draw_info(info, ctx)


def draw(data_join: typing.Sequence, destination: str, base: str=None):
	with tempfile.TemporaryDirectory() as tmpdir:
		pdf_writer = pypdf.PdfWriter()
		if base:
			pdf_base = pypdf.PdfReader(base)

		for idx_page, page_info in enumerate(data_join):
			logger.info("Filling page: %d", idx_page+1)
			path = os.path.join(tmpdir, f"{idx_page:04d}.pdf")
			add_page(page_info, path)

			page_filled = pypdf.PdfReader(path).pages[0]
			if base:
				page_base = pdf_base.pages[idx_page]
				page_base.merge_page(page_filled)
				page_filled = page_base

			pdf_writer.add_page(page_filled)

		pdf_writer.write(destination)
		pdf_writer.close()
